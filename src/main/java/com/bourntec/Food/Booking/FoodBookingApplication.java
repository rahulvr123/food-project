package com.bourntec.Food.Booking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodBookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodBookingApplication.class, args);
	}

}
